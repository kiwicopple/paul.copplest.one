

<p style="font-size: 1.1rem; margin: 60px 0;">Full Stack Developer and Entrepreneur.</p>

----


[Businesses and Projects](/projects/) <br />Projects that I am (or was) involved with.

[Blog](/blog/) <br />Off topic posts or opinions.

[Principles](/principles/) <br />Inspired by Ray Dalio's Book "Principles". Principles are ways of successfully dealing with reality to get what you want out of life.

[Knowledge](/knowledge/) <br />A repository of personal knowledge that I update from time to time. These small changes are like compound interest - I expect them to be useful and accurate after an entire lifetime of updates.

[Gists](/gists/) <br />A list of code snippets and tips that I use across my projects, servers, or dev machines.

**Find me online** <br />[LinkedIn](https://www.linkedin.com/in/paulcopplestone/) | [GitHub](https://github.com/kiwicopple) | [GitLab](https://gitlab.com/kiwicopple) 