--- 
date: 2019-11-10
description: The unreasonable effectiveness of doing nothing.
---
# Do nothing

The unreasonable effectiveness of doing nothing.

**A typical interaction.**

I had a colleague send me a message the other day:

>  12:13pm colleague: hi copple

Me: *spend 2 minutes considering how I can politely suggest a better way of communicating asynchonously.*

> 12:15pm me: hi, how can I help?

*One minute later*

> 12:16pm colleague: can i check if stripe integration has been removed from xero? cause a client is trying to pay for an invoice via credit card but the button is no longer there

*Me: do nothing.*

> 12:28pm colleague: ah no worries - it's sorted already


**Why you should do nothing.**

I spent my early career as a sysadmin in a company of about 300 people. These interactions where absurdly frequent. Being an young upstart I would jump on them straight away.

Often I would spend hours solving the problem, prioritising it above my own work, only to find it wasn't even important to begin with.

The thing about accepting these requests is that it removes a burden from the requestor. They have some stress, and they need someone to offload that stress on. This has nothing to do with the actual problem and everything to do with the person's peace of mind.

At some point we realise that we aren't paid to be psychologists, we are paid to solve problems in the order of most important first. 

When the problem is important enough, you will *know*. They will call you. They won't start a message with a simple "hey {developer}". 

If has gotten to that stage the requestor knows in the back of their head that there isn't anything that they can try. They have tried everything themselves, they are stuck, and it's a problem that actually needs solving.

Most of the time it won't even get to this stage. Perhaps the problems solve themselves, perhaps the requestor realises they weren't really problems to begin with. 

**Do less.**

Make sure you cut out the signal from the noise. Sometimes the best course of action is no action at all.