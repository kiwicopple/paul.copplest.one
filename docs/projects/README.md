---
sidebar: true
---

## Businesses 

[Nimbus For Work](/projects/nimbus-for-work.html)

[ServisHero](/projects/servishero.html)

## Side Projects

[Quick, Font!](/projects/quick-font.html)

[Sheet Metal](/projects/sheetmetal.html)

[Pollygot](/projects/pollygot.html)

[Mental Models](/projects/mentalmodels.html)

[Buy Meth](/projects/buymeth.html)

[Driffft](/projects/driffft.html)

[BraineeBox](/projects/braineebox.html)

