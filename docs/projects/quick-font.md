---
description: Tools for Developers
---

# Quick, Font!

Some good fonts for prototyping or snazzing up an internal website (without having to go through the pain of downloading or installing).

## Links

- Homepage: [https://quickfont.xyz](https://quickfont.xyz)
- Github: [https://github.com/kiwicopple/quick-font](https://github.com/kiwicopple/quick-font)
