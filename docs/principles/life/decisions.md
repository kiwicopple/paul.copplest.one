---
date: 2019-12-13
description: Decision making framework for life events
---

# Decisions

**Contents**

[[toc]]

## Minimise regret

Make decisions based on the least possible regret.

## Always try your best

@todo

## Always be honest

@todo

## Always be wholesome

@todo