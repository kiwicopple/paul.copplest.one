# Seed

Priciples for operating from Angel to Seed stages.


## The game

Product market fit.

- Low competition markets: Solving a problem/need. Minimum Viable Product.
- High competition markets: Building a better product. Minimum Loveable Product.


## The process

- Adaptability
- Ignore the minutae - everything is 80/20

## How to spend money

Spend as if you will never be able to raise again.


## Resources

- [Version One startup handbook](https://versionone.vc/startup-handbook/)